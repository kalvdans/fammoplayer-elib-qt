#ifndef NETWORKLIBRARY_H
#define NETWORKLIBRARY_H

#include <QObject>
#include <QProcess>
#include <QString>
#include <QSet>
#include <QTimer>
#include <QQueue>
#include <QHash>
#include <QVariant>

class Library : public QObject
{
  Q_OBJECT
public:
  explicit Library(QObject *parent = 0);
  void start();

signals:
  void indexUpdated(QSet<QString> index);
  void entriesCached(QSet<QString> entries);
  void entriesDeleted(QSet<QString> entries);

public slots:
  void fetchIndex();
  void cacheEntries(QSet<QString> entries);
  void pruneLocalRepository(const QStringList &prio);

private slots:
  void onFetchFinished(int exitCode, QProcess::ExitStatus exitStatus);
  void onRsyncFinished(int exitCode, QProcess::ExitStatus exitStatus);
  void startSync();

private:

  QTimer m_timer;
  QProcess m_rsync, m_find;
  QQueue<QString> m_syncQueue;
  QHash<QString, QVariant> m_bookSizes;
  qulonglong m_totalSize = 0;

};

#endif // NETWORKLIBRARY_H
