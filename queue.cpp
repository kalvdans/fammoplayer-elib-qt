#include "queue.h"

#include <QSettings>
#include <QDebug>

Queue::Queue()
{
}

void Queue::nextBook()
{
  setCurrentBook(m_currentIndex + 1);
}

void Queue::prevBook()
{
  setCurrentBook(m_currentIndex - 1);
}

void Queue::setCurrentBook(int index)
{
  index = qBound(0, index, m_queue.size() - 1);

  const QString & nextBook = m_queue[m_currentIndex];

  emit cacheBooks(QSet<QString>() << nextBook);
  emit cacheBooks(m_queue.mid(index - 1, 5).toSet() - m_cached);

  if (m_cached.contains(nextBook))
  {
    m_currentIndex = index;
    emit bookChanged(nextBook);
  }
}

void Queue::onIndexUpdated(QSet<QString> index)
{
  QSet<QString> deleted = m_index - index;
  QSet<QString> deleted_and_not_cached = deleted - m_cached;
  QSet<QString> added = index - m_index;

  QString currentBook;
  if (m_queue.size() > 0)
    currentBook = m_queue[m_currentIndex];

  for (const QString& entry : deleted_and_not_cached)
    m_queue.removeOne(entry);
  for (const QString& entry : added)
    m_queue.push_back(entry);

  if (!currentBook.isNull())
    m_currentIndex = m_queue.indexOf(currentBook);

  emit cacheBooks(m_queue.mid(m_currentIndex, 5).toSet() - m_cached);

//  qDebug() << "Queue:" << m_queue;
  m_index = index;
}

void Queue::onQueueUpdated(QStringList queue)
{
  if (m_queue != queue)
  {
    queue.removeDuplicates();

    QSet<QString> queueSet(queue.toSet());

    QSet<QString> nonExisting = queueSet - m_index;

    for (const QString& entry : nonExisting)
      m_queue.removeOne(entry);

    m_queue = queue;
  }
}

void Queue::onEntriesCached(QSet<QString> entries)
{
  m_cached.unite(entries);

  qDebug() << "Currently cached:" << m_cached;

  QStringList prio;
  if (m_currentIndex > 0)
    prio = m_queue.mid(0, m_currentIndex - 1);
  for (int i = m_queue.size() - 1; i >= 0; i--)
    if (m_cached.contains(m_queue[i]))
      prio << m_queue[i];

  emit pruneLocalBooks(prio);

}

void Queue::onEntriesDeleted(QSet<QString> entries)
{
  m_cached.subtract(entries);
  qDebug() << "Currently cached:" << m_cached;
}
