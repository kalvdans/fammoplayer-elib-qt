#include "input.h"
#include "device.h"

#include <QStringList>

Input::Input(QObject *parent) :
  QObject(parent)
{
  for (const QString & deviceName : QStringList() << "SynPS/2 Synaptics TouchPad")
  {
    Device *d = Device::getDevice("SynPS/2 Synaptics TouchPad");
  }
}

void Input::signal(Input::Signal s)
{
  switch (s) {
  case Play:
    emit play();
    break;
  case Pause:
    emit pause();
    break;
  case NextTrack:
    emit nextTrack();
    break;
  case PrevTrack:
    emit prevTrack();
    break;
  default:
    break;
  }
}
