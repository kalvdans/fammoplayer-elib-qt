#include "player.h"

#include <QDir>
#include <QMediaContent>
#include <QDebug>

Player::Player(QObject *parent) :
  QObject(parent)
{
  connect(&m_player, &QMediaPlayer::stateChanged, this, &Player::onMediaPlayerStateChange);
  m_playList.setPlaybackMode(QMediaPlaylist::CurrentItemOnce);
  m_player.setPlaylist(&m_playList);
}

void Player::setBook(QString book)
{
  QDir dir = QDir("/home/mhn/Downloads").filePath(book);

  QStringList files = dir.entryList(QStringList() << "*.mp3" << "*.ogg" << "*.flac", QDir::Files | QDir::Readable, QDir::Name);

  QList<QMediaContent> tracks;
  for (const QString& file : files)
  {
    tracks << QMediaContent(QUrl::fromLocalFile(dir.filePath(file)));
  }

  m_playList.removeMedia(0, m_playList.mediaCount() - 1);

  if (!m_playList.addMedia(tracks))
    qWarning() << "Could not add media";
  else
    qDebug() << "Added media";

  for (int i = 0; i < m_playList.mediaCount(); i++)
  {
    const QMediaContent& media = m_playList.media(i);
    qDebug() << "playlist" << i << media.canonicalUrl();
  }

  if (m_currentTrack < 0)
    m_currentTrack = m_playList.mediaCount() - 1;
  else
    m_currentTrack = 0;

  m_playList.setCurrentIndex(m_currentTrack);

//  m_player.setPlaybackRate(20.0);
  m_player.play();
}

void Player::play()
{
  m_player.play();
//  switch (m_player.state()) {
//  case QMediaPlayer::PlayingState:
//    m_player.pause();
//    break;
//  default:
//    m_player.play();
//    break;
//  }
}

void Player::pause()
{
  m_player.pause();
}

void Player::nextTrack()
{
  if (++m_currentTrack >= m_playList.mediaCount())
  {
    emit nextBook();
  }
  else
  {
    m_playList.setCurrentIndex(m_currentTrack);
  }

//  switch (m_player.state()) {
//  case QMediaPlayer::StoppedState:
//    m_playList.setCurrentIndex(m_currentTrack);
//    break;
//  default:
//    m_playList.setCurrentIndex(m_currentTrack + 1);
//    break;
//  }
//  m_player.play();
}

void Player::prevTrack()
{
  if (--m_currentTrack < 0)
  {
    emit prevBook();
  }
  else
  {
    m_playList.setCurrentIndex(m_currentTrack);
  }
//  switch (m_player.state()) {
//  case QMediaPlayer::PausedState:
//    m_playList.setCurrentIndex(m_currentTrack);
//    break;
//  default:
//    m_playList.setCurrentIndex(m_currentTrack - 1);
//    break;
//  }
//  m_player.play();
}

void Player::changeVolume(int delta)
{
  m_player.setVolume(m_player.volume() + delta);
}

void Player::onMediaPlayerStateChange(QMediaPlayer::State state)
{
  qDebug() << "player state:" << state << m_playList.currentIndex() << m_player.mediaStatus();

  if (state == QMediaPlayer::StoppedState)
    emit nextTrack();

  emit stateChanged(Player::State(state));
}
