#include "device.h"
#include <QDir>
#include <QFile>
#include <QDebug>

Device::Device(QString path, QObject *parent) :
  QObject(parent),
  m_dev(path)
{
  m_dev.open(QFile::ReadOnly);
  libevdev_new_from_fd(m_dev.handle(), &m_evdev);
}

Device::~Device()
{
  libevdev_free(m_evdev);
}

Device* Device::getDevice(QString name)
{
  QDir eventdir("/dev/input", "event*", QDir::Unsorted, QDir::System | QDir::Readable);

  QStringList devices = eventdir.entryList();

  qDebug() << "devices:" << devices;
  for (const QString &device : devices)
  {
    QFile dev(eventdir.filePath(device));
    if (dev.open(QFile::ReadOnly))
    {
      libevdev *evdev;
      int rc = libevdev_new_from_fd(dev.handle(), &evdev);
      if (rc == 0)
      {
        QString evname = libevdev_get_name(evdev);
        libevdev_free(evdev);
        if (name == evname)
          return new Device(dev.fileName());
      }
    }
  }
  return nullptr;
}
