#ifndef MOUSE_H
#define MOUSE_H

#include <QObject>
#include <QFile>
#include <libevdev/libevdev.h>

class Device : public QObject
{
  Q_OBJECT
public:
  ~Device();

  static Device *getDevice(QString name);

private:
  explicit Device(QString path, QObject *parent = 0);
signals:

public slots:

private:
  libevdev *m_evdev;
  QFile m_dev;
};

#endif // MOUSE_H
