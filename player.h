#ifndef ALBUMPLAYER_H
#define ALBUMPLAYER_H

#include <QObject>
#include <QMediaPlaylist>
#include <QMediaPlayer>

class Player : public QObject
{
  Q_OBJECT
public:
  enum State {StoppedState, PlayingState, PausedState};

  explicit Player(QObject *parent = 0);

signals:
  void nextBook();
  void prevBook();
  void stateChanged(State state);

public slots:
  void setBook(QString book);
  void play();
  void pause();
  void nextTrack();
  void prevTrack();
  void changeVolume(int delta);

private slots:
  void onMediaPlayerStateChange(QMediaPlayer::State state);

private:
  QMediaPlaylist m_playList;
  QMediaPlayer m_player;

  int m_currentTrack = 0;
};

#endif // ALBUMPLAYER_H
