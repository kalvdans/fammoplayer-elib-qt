#ifndef INPUT_H
#define INPUT_H

#include <QObject>
#include <QList>

class Device;

class Input : public QObject
{
  Q_OBJECT

  enum Signal {Play, Pause, Stop, NextTrack, PrevTrack, NextBook, PrevBook};

public:
  explicit Input(QObject *parent = 0);

signals:
  void play();
  void pause();
  void nextTrack();
  void prevTrack();
  void changeVolume(int delta);

public slots:
  void signal(Signal s);

private:
  QList<Device*> m_devices;
};

#endif // INPUT_H
