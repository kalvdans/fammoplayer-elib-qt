#include <QCoreApplication>

#include "player.h"
#include "library.h"
#include "queue.h"
#include "input.h"

int main(int argc, char *argv[])
{
  QCoreApplication::setOrganizationName("Fammo");
  QCoreApplication::setApplicationName("ElibPlayer-Qt");
  QCoreApplication::setApplicationVersion("2.0");

//  Device *d = Device::getDevice("SynPS/2 Synaptics TouchPad");

//  delete d;

  QCoreApplication a(argc, argv);

  Library l;
  Player  p;
  Queue   q;
  Input   i;

  QObject::connect(&l, &Library::entriesCached,  &q, &Queue::onEntriesCached);
  QObject::connect(&l, &Library::entriesDeleted, &q, &Queue::onEntriesDeleted);
  QObject::connect(&l, &Library::indexUpdated,   &q, &Queue::onIndexUpdated);

  QObject::connect(&q, &Queue::pruneLocalBooks,  &l, &Library::pruneLocalRepository);
  QObject::connect(&q, &Queue::cacheBooks,       &l, &Library::cacheEntries);


  QObject::connect(&q, &Queue::bookChanged, &p, &Player::setBook);

  QObject::connect(&p, &Player::nextBook,   &q, &Queue::nextBook);
  QObject::connect(&p, &Player::prevBook,   &q, &Queue::prevBook);

//  for (const QString & deviceName : QStringList() << "SynPS/2 Synaptics TouchPad")
//  {
//    Device *d = Device::getDevice("SynPS/2 Synaptics TouchPad");
//  }

  l.start();
//  QTimer::singleShot(100, &l, SLOT(fetchIndex()));

  return a.exec();
}
