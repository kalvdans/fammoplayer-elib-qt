#-------------------------------------------------
#
# Project created by QtCreator 2013-06-23T11:40:43
#
#-------------------------------------------------

QT       += core multimedia

QT       -= gui

TARGET = elibplayer-qt
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    player.cpp \
    queue.cpp \
    library.cpp \
    input.cpp \
    device.cpp

HEADERS += \
    player.h \
    queue.h \
    library.h \
    input.h \
    device.h

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += taglib libevdev
