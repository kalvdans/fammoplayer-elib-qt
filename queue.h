#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <QObject>
#include <QSet>
#include <QString>
#include <QStringList>

class Queue : public QObject
{
  Q_OBJECT
public:
  explicit Queue();

signals:
  void bookChanged(QString entry);
  void cacheBooks(QSet<QString> entries);
  void pruneLocalBooks(QStringList entries);

public slots:
  void nextBook();
  void prevBook();

  void onIndexUpdated(QSet<QString> index);
  void onQueueUpdated(QStringList queue);
  void onEntriesCached(QSet<QString> entries);
  void onEntriesDeleted(QSet<QString> entries);

  void setCurrentBook(int index);

private:
  QStringList m_queue;
  QSet<QString> m_cached;
  QSet<QString> m_index;
  int m_currentIndex = 0;
};

#endif // PLAYLIST_H
